package ru.panasyuk.tm;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.AuthRestEndpointClient;
import ru.panasyuk.tm.client.TaskRestEndpointClient;
import ru.panasyuk.tm.client.TasksRestEndpointClient;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class TasksRestEndpointTest {

    @NotNull
    static TaskRestEndpointClient client;

    @NotNull
    static TasksRestEndpointClient tasksClient;

    @NotNull
    final List<Task> tasks = new ArrayList<>();

    @NotNull
    static String testUserId;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        @NotNull final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        @NotNull final AuthRestEndpointClient authRestEndpointClient = AuthRestEndpointClient.client(builder);
        authRestEndpointClient.login("test", "test");
        testUserId = authRestEndpointClient.profile().getId();

        client = TaskRestEndpointClient.client(builder);
        tasksClient = TasksRestEndpointClient.client(builder);
    }

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Task task = new Task("Task " + i);
            tasks.add(client.create(task));
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
        }
        tasks.clear();
    }

    @Test
    public void testCount() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 1;
        @NotNull final Task task = new Task("Task Test");
        client.create(task);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(task);
    }

    @Test
    public void testDeleteAll() {
        final long taskSize = tasksClient.count();
        Assert.assertTrue(taskSize > 0);
        tasksClient.deleteAll();
        final long newTaskSize = tasksClient.count();
        Assert.assertEquals(0, newTaskSize);
    }

    @Test
    public void testFindAll() {
        final long taskSize = tasksClient.count();
        @Nullable final List<Task> foundTasks = tasksClient.findAll();
        Assert.assertNotNull(foundTasks);
        Assert.assertEquals(foundTasks.size(), taskSize);
    }

    @Test
    public void testSaveAll() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 2;
        @NotNull final Task taskTest1 = new Task("Task Test 1");
        taskTest1.setUserId(testUserId);
        @NotNull final Task taskTest2 = new Task("Task Test 2");
        taskTest2.setUserId(testUserId);
        @NotNull final List<Task> testTasks = new ArrayList<>();
        testTasks.add(taskTest1);
        testTasks.add(taskTest2);
        tasksClient.saveAll(testTasks);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(taskTest1);
        client.delete(taskTest2);
    }

}