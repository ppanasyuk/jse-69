package ru.panasyuk.tm;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.*;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    static TaskRestEndpointClient client;

    @NotNull
    static TasksRestEndpointClient tasksClient;

    @NotNull
    final List<Task> tasks = new ArrayList<>();

    @NotNull
    static String testUserId;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        @NotNull final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        @NotNull final AuthRestEndpointClient authRestEndpointClient = AuthRestEndpointClient.client(builder);
        authRestEndpointClient.login("test", "test");
        testUserId = authRestEndpointClient.profile().getId();

        client = TaskRestEndpointClient.client(builder);
        tasksClient = TasksRestEndpointClient.client(builder);
    }

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Task task = new Task("Task " + i);
            tasks.add(client.create(task));
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
        }
        tasks.clear();
    }

    @Test
    public void testSave() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 1;
        @NotNull final Task taskTest = new Task("Task Test");
        taskTest.setUserId(testUserId);
        @Nullable final Task savedTask = client.save(taskTest);
        Assert.assertNotNull(savedTask);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(taskTest);
    }

    @Test
    public void testFindById() {
        for (@NotNull final Task task : tasks) {
            @Nullable final Task foundTask = client.findById(task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testDeleteById() {
        for (@NotNull final Task task : tasks) {
            client.deleteById(task.getId());
            @Nullable final Task deletedTask = client.findById(task.getId());
            Assert.assertNull(deletedTask);
        }
    }

    @Test
    public void testDelete() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
            @Nullable final Task deletedTask = client.findById(task.getId());
            Assert.assertNull(deletedTask);
        }
    }

}