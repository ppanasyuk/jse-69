package ru.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    Project save(@NotNull Project project);

    public void saveAll(@NotNull List<Project> projects);

    void saveAllByUserId(@NotNull String userId, @NotNull List<Project> projects);

    @NotNull
    Project saveByUserId(@NotNull String userId, @NotNull Project project);

    @Nullable
    Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findById(@NotNull String id);

    boolean existByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void delete(@NotNull Project project);

    void clear(@NotNull List<Project> projects);

    void clearByUserId(@NotNull String userId);

    void clearAllByUserId(@NotNull String userId, @NotNull List<Project> projects);

}