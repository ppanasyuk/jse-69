package ru.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId);

    @NotNull
    Task save(@NotNull Task project);

    void saveAll(@NotNull List<Task> projects);

    void saveAllByUserId(@NotNull String userId, @NotNull List<Task> tasks);

    Task saveByUserId(@NotNull String userId, @NotNull Task task);

    @Nullable
    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void delete(@NotNull Task project);

    void clear(@NotNull List<Task> projects);

    void clearByUserId(@NotNull String userId);

    void clearAllByUserId(@NotNull String userId, @NotNull List<Task> tasks);

}