package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

public interface IProjectEndpoint {

    @NotNull
    Project create(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Project project
    );

    void clear(
            @AuthenticationPrincipal CustomUser user
    );

    long count(
            @AuthenticationPrincipal CustomUser user
    );

    void deleteById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    void delete(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Project project
    );

    void deleteAll(
            @AuthenticationPrincipal CustomUser user,
            @NotNull List<Project> projects
    );

    boolean existById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    @Nullable
    List<Project> findAll(
            @AuthenticationPrincipal CustomUser user
    );

    @Nullable
    Project findById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    @NotNull
    Project save(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Project project
    );

    void saveAll(
            @AuthenticationPrincipal CustomUser user,
            @NotNull List<Project> projects
    );

}