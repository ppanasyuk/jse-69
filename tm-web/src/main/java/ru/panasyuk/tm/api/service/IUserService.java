package ru.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.User;

import java.util.List;

public interface IUserService {

    @Nullable
    List<User> findAll();

    @NotNull
    User save(@NotNull User user);

    public void saveAll(@NotNull List<User> users);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    boolean existById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull User user);

    void clear(@NotNull List<User> users);

    void clear();

}