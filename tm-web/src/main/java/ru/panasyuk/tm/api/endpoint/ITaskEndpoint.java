package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    Task create(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Task task
    );

    void clear(
            @AuthenticationPrincipal CustomUser user
    );

    long count(
            @AuthenticationPrincipal CustomUser user
    );

    void deleteById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    void delete(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Task task
    );

    void deleteAll(
            @AuthenticationPrincipal CustomUser user,
            @NotNull List<Task> tasks
    );

    boolean existById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    @Nullable
    List<Task> findAll(
            @AuthenticationPrincipal CustomUser user
    );

    @Nullable
    Task findById(
            @AuthenticationPrincipal CustomUser user,
            @NotNull String id
    );

    @NotNull
    Task save(
            @AuthenticationPrincipal CustomUser user,
            @NotNull Task task
    );

    void saveAll(
            @AuthenticationPrincipal CustomUser user,
            @NotNull List<Task> tasks
    );

}