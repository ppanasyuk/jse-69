package ru.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Result;
import ru.panasyuk.tm.model.User;

import java.util.List;

public interface IAuthService {

    @NotNull
    Result login(@NotNull String username, @NotNull String password);

    @Nullable
    User profile();

    @NotNull
    Result logout();

}