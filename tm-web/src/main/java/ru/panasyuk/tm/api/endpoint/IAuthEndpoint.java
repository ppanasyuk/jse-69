package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.panasyuk.tm.model.Result;
import ru.panasyuk.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @WebMethod
    Result login(
            @WebParam(name = "username", partName = "username")
            @NotNull String username,
            @WebParam(name = "password", partName = "password")
            @NotNull String password
    );

    @WebMethod
    User profile();

    @WebMethod
    Result logout();

}