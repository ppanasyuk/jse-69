package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.panasyuk.tm.api.service.IAuthService;
import ru.panasyuk.tm.dto.soap.*;

@Endpoint
public class AuthSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "AuthSoapEndpointPort";

    public static final String NAMESPACE = "http://panasyuk.ru/tm/dto/soap";

    @Autowired
    private IAuthService authService;

    @ResponsePayload
    @PreAuthorize("permitAll()")
    @PayloadRoot(localPart = "authLoginRequest", namespace = NAMESPACE)
    public AuthLoginResponse login(@RequestPayload final AuthLoginRequest request) {
        @NotNull final String username = request.getUsername();
        @NotNull final String password = request.getPassword();
        return new AuthLoginResponse(authService.login(username, password));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "authProfileRequest", namespace = NAMESPACE)
    public AuthProfileResponse profile(@RequestPayload final AuthProfileRequest request) {
        return new AuthProfileResponse(authService.profile());
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "authLogoutRequest", namespace = NAMESPACE)
    public AuthLogoutResponse logout(@RequestPayload final AuthLogoutRequest request) {
        return new AuthLogoutResponse(authService.logout());
    }

}