package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.ITaskEndpoint;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Task;

import javax.jws.WebMethod;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @PostMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public Task create(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull final Task task
    ) {
        task.setUserId(user.getUserId());
        return taskService.save(task);
    }

    @WebMethod
    @PostMapping("/clear")
    @PreAuthorize("isAuthenticated()")
    public void clear(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.clearByUserId(user.getUserId());
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count(
            @AuthenticationPrincipal final CustomUser user
    ) {
        return taskService.countByUserId(user.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id
    ) {
        taskService.deleteByUserIdAndId(user.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final Task task
    ) {
        taskService.deleteByUserIdAndId(user.getUserId(), task.getId());
    }

    @Override
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void deleteAll(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final List<Task> tasks
    ) {
        taskService.clearAllByUserId(user.getUserId(), tasks);
    }

    @Override
    @GetMapping("/existById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.existByUserIdAndId(user.getUserId(), id);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<Task> findAll(
            @AuthenticationPrincipal final CustomUser user
    ) {
        return taskService.findAllByUserId(user.getUserId());
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public Task findById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.findByUserIdAndId(user.getUserId(), id);
    }

    @NotNull
    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/save", produces = "application/json")
    public Task save(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final Task task
    ) {
        return taskService.saveByUserId(user.getUserId(), task);
    }

    @Override
    @PostMapping("/saveAll")
    @PreAuthorize("isAuthenticated()")
    public void saveAll(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final List<Task> tasks) {
        taskService.saveAllByUserId(user.getUserId(), tasks);
    }

}