package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.dto.soap.*;
import ru.panasyuk.tm.model.Project;
import ru.panasyuk.tm.util.UserUtil;

@Endpoint
public class ProjectSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public static final String NAMESPACE = "http://panasyuk.ru/tm/dto/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.clearByUserId(UserUtil.getUserId());
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        return new ProjectCountResponse(projectService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@RequestPayload final ProjectDeleteAllRequest request) {
        projectService.clearAllByUserId(UserUtil.getUserId(), request.getProjects());
        return new ProjectDeleteAllResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.deleteByUserIdAndId(UserUtil.getUserId(), request.getProject().getId());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectExistByIdRequest", namespace = NAMESPACE)
    public ProjectExistByIdResponse existById(@RequestPayload final ProjectExistByIdRequest request) {
        return new ProjectExistByIdResponse(projectService.existByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectService.findByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        @NotNull Project project = new Project();
        project.setId(request.getProject().getId());
        project.setName(request.getProject().getName());
        project.setDescription(request.getProject().getDescription());
        project.setStatus(request.getProject().getStatus());
        project.setDateStart(request.getProject().getDateStart());
        project.setDateFinish(request.getProject().getDateFinish());
        project.setUserId(request.getProject().getUserId());
        projectService.saveByUserId(UserUtil.getUserId(), project);
        return new ProjectSaveResponse(project);
    }

}