package ru.panasyuk.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.dto.soap.*;
import ru.panasyuk.tm.model.Task;
import ru.panasyuk.tm.util.UserUtil;

@Endpoint
public class TaskSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public static final String NAMESPACE = "http://panasyuk.ru/tm/dto/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.clearByUserId(UserUtil.getUserId());
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        return new TaskCountResponse(taskService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(@RequestPayload final TaskDeleteAllRequest request) {
        taskService.clearAllByUserId(UserUtil.getUserId(), request.getTasks());
        return new TaskDeleteAllResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), request.getTask().getId());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskExistByIdRequest", namespace = NAMESPACE)
    public TaskExistByIdResponse existById(@RequestPayload final TaskExistByIdRequest request) {
        return new TaskExistByIdResponse(taskService.existByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        return new TaskFindAllResponse(taskService.findAllByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        Task task = new Task();
        task.setId(request.getTask().getId());
        task.setName(request.getTask().getName());
        task.setDescription(request.getTask().getDescription());
        task.setStatus(request.getTask().getStatus());
        task.setDateStart(request.getTask().getDateStart());
        task.setDateFinish(request.getTask().getDateFinish());
        task.setProjectId(request.getTask().getProjectId());
        task.setUserId(request.getTask().getUserId());
        taskService.saveByUserId(UserUtil.getUserId(), task);
        return new TaskSaveResponse(request.getTask());
    }

}