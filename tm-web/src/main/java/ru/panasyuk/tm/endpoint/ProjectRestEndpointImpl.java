package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @PostMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public Project create(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull final Project project) {
        project.setUserId(user.getUserId());
        return projectService.save(project);
    }

    @Override
    @PostMapping("/clear")
    @PreAuthorize("isAuthenticated()")
    public void clear(
            @AuthenticationPrincipal final CustomUser user
    ) {
        projectService.clearByUserId(user.getUserId());
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count(
            @AuthenticationPrincipal final CustomUser user
    ) {
        return projectService.countByUserId(user.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id) {
        projectService.deleteByUserIdAndId(user.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final Project project
    ) {
        projectService.deleteByUserIdAndId(user.getUserId(), project.getId());
    }

    @Override
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void deleteAll(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final List<Project> projects) {
        projectService.clearAllByUserId(user.getUserId(), projects);
    }

    @Override
    @GetMapping("/existById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id) {
        return projectService.existByUserIdAndId(user.getUserId(), id);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<Project> findAll(
            @AuthenticationPrincipal final CustomUser user
    ) {
        return projectService.findAllByUserId(user.getUserId());
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public Project findById(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") @NotNull final String id
    ) {
        return projectService.findByUserIdAndId(user.getUserId(), id);
    }

    @NotNull
    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/save", produces = "application/json")
    public Project save(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final Project project
    ) {
        return projectService.saveByUserId(user.getUserId(), project);
    }

    @Override
    @PostMapping("/saveAll")
    @PreAuthorize("isAuthenticated()")
    public void saveAll(
            @AuthenticationPrincipal final CustomUser user,
            @RequestBody @NotNull final List<Project> projects
    ) {
        projectService.saveAllByUserId(user.getUserId(), projects);
    }

}