package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.panasyuk.tm.api.service.IAuthService;
import ru.panasyuk.tm.model.Result;
import ru.panasyuk.tm.model.User;

@RestController
@RequestMapping("api/auth")
public class AuthRestEndpointImpl implements IAuthEndpoint {

    @Autowired
    private IAuthService authService;

    @Override
    @PreAuthorize("permitAll()")
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(@NotNull final String username, @NotNull final String password) {
        return authService.login(username, password);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public User profile() {
        return authService.profile();
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result logout() {
        return authService.logout();
    }

}