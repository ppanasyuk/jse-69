package ru.panasyuk.tm.client;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.panasyuk.tm.model.Result;
import ru.panasyuk.tm.model.User;

import java.net.CookieManager;
import java.net.CookiePolicy;

@FeignClient
public interface AuthRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/auth";

    static AuthRestEndpointClient client() {
        @NotNull final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return client(builder);
    }

    static AuthRestEndpointClient client(@NotNull final OkHttpClient.Builder builder) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        return Feign.builder()
                .client(new feign.okhttp.OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("login")
    Result login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    );

    @GetMapping("profile")
    User profile();

    @GetMapping("logout")
    void logout();

}