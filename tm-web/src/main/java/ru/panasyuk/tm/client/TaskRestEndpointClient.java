package ru.panasyuk.tm.client;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.panasyuk.tm.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;

@FeignClient
public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return client(builder);
    }

    static TaskRestEndpointClient client(@NotNull final OkHttpClient.Builder builder) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        return Feign.builder()
                .client(new feign.okhttp.OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @PostMapping(value = "/create", produces = "application/json")
    Task create(@RequestBody Task project);

    @GetMapping("findById/{id}")
    Task findById(@PathVariable("id") String id);

    @PostMapping("deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("delete")
    void delete(@RequestBody Task project);

    @PostMapping(value = "/save", produces = "application/json")
    Task save(@RequestBody Task project);

}