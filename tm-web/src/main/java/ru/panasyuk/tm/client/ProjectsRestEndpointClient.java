package ru.panasyuk.tm.client;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.model.Project;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

@FeignClient
public interface ProjectsRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectsRestEndpointClient client() {
        @NotNull final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return client(builder);
    }

    static ProjectsRestEndpointClient client(@NotNull final OkHttpClient.Builder builder) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        return Feign.builder()
                .client(new feign.okhttp.OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectsRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("count")
    long count();

    @PostMapping("saveAll")
    void saveAll(@RequestBody List<Project> projects);

    @GetMapping("findAll")
    List<Project> findAll();

    @PostMapping("clear")
    void deleteAll();

}