package ru.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findByUserId(@NotNull String userId);

    @Nullable
    Task findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

}