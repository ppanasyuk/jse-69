package ru.panasyuk.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.panasyuk.tm.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(@Nullable String login);

}