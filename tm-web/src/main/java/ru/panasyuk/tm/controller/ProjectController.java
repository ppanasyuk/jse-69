package ru.panasyuk.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.enumerated.Status;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @PostMapping("/project/create")
    @PreAuthorize("isAuthenticated()")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        @NotNull final Project project = new Project("Project " + System.currentTimeMillis());
        project.setUserId(user.getUserId());
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Project project = projectService.findByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}