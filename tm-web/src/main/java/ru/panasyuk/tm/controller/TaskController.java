package ru.panasyuk.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.enumerated.Status;
import ru.panasyuk.tm.model.CustomUser;
import ru.panasyuk.tm.model.Project;
import ru.panasyuk.tm.model.Task;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @PostMapping("/task/create")
    @PreAuthorize("isAuthenticated()")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        @NotNull final Task task = new Task("Task " + System.currentTimeMillis());
        task.setUserId(user.getUserId());
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (task.getProjectId() != null && task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user.getUserId()));
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    private Collection<Project> getProjects(@NotNull final String userId) {
        return projectService.findAllByUserId(userId);
    }

}