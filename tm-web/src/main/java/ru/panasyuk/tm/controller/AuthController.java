package ru.panasyuk.tm.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {

    @GetMapping("/login")
    @PreAuthorize("permitAll()")
    public String login() {
        return "login";
    }

    @GetMapping("/auth")
    @PreAuthorize("permitAll()")
    public String auth() {
        return "index";
    }

    @GetMapping("/logout")
    @PreAuthorize("isAuthenticated()")
    public String logout() {
        return "login";
    }

}