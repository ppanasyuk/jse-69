package ru.panasyuk.tm.dto.soap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ResultDto {

    private static final long serialVersionUID = 1;

    private Boolean success = true;

    private String message = "";

    public ResultDto() {
    }

    public ResultDto(@NotNull final Boolean success) {
        this.success = success;
    }

    public ResultDto(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}