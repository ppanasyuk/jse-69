package ru.panasyuk.tm.dto.soap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.panasyuk.tm.constant.DBConst;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = DBConst.TABLE_USER)
public class UserDto {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = DBConst.COLUMN_ID, length = 36, nullable = false, updatable = false)
    private String id = UUID.randomUUID().toString();

    @Column(name = DBConst.COLUMN_LOGIN, nullable = false, updatable = false)
    private String login;

    @Column(name = DBConst.COLUMN_PASSWORD, nullable = false)
    private String passwordHash;

}