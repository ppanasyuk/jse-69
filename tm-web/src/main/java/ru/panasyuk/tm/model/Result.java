package ru.panasyuk.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class Result {

    private static final long serialVersionUID = 1;

    private Boolean success = true;

    private String message = "";

    public Result() {
    }

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}