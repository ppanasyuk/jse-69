package ru.panasyuk.tm.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.panasyuk.tm.constant.DBConst;
import ru.panasyuk.tm.enumerated.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_ROLE)
public class Role {

    private static final long serialVersionUID = 1;

    @Id
    @Column(name = DBConst.COLUMN_ID, length = 36, nullable = false, updatable = false)
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JsonIgnore
    @XmlTransient
    @JoinColumn(name = DBConst.COLUMN_USER_ID)
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_ROLE_TYPE, length = 30)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

    @XmlTransient
    public User getUser() {
        return user;
    }

}