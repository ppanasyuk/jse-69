package ru.panasyuk.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class Message {

    @NotNull
    private String value;

    public Message(@NotNull final String value) {
        this.value = value;
    }

}