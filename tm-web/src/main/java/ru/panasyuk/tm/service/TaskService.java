package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.model.Task;
import ru.panasyuk.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        return taskRepository.findByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Task save(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@NotNull final List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void saveAllByUserId(@NotNull final String userId, @NotNull final List<Task> tasks) {
        for (@NotNull final Task task : tasks)
            if (!userId.equals(task.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        taskRepository.saveAll(tasks);
    }

    @NotNull
    @Override
    @Transactional
    public Task saveByUserId(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    public Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return taskRepository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    public boolean existByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return taskRepository.findFirstByUserIdAndId(userId, id) != null;
    }

    @Override
    public long countByUserId(@NotNull final String userId) {
        return taskRepository.countByUserId(userId);
    }

    @Transactional
    public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void clear(@NotNull final List<Task> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @Transactional
    public void clearByUserId(@NotNull final String userId) {
        taskRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void clearAllByUserId(@NotNull final String userId, @NotNull final List<Task> tasks) {
        for (@NotNull final Task task : tasks)
            if (!userId.equals(task.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        taskRepository.deleteAll(tasks);
    }

}