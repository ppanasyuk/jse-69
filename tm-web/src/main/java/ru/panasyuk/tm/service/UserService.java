package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.tm.api.service.IUserService;
import ru.panasyuk.tm.model.User;
import ru.panasyuk.tm.repository.UserRepository;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @Override
    @Transactional
    public User save(@NotNull final User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public void saveAll(@NotNull final List<User> users) {
        userRepository.saveAll(users);
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return userRepository.existsById(id);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final User user) {
        userRepository.delete(user);
    }

    @Override
    @Transactional
    public void clear(@NotNull final List<User> users) {
        userRepository.deleteAll(users);
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

}