package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.model.Project;
import ru.panasyuk.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return projectRepository.findByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Project save(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void saveAll(@NotNull final List<Project> projects) {
        projectRepository.saveAll(projects);
    }

    @Override
    @Transactional
    public void saveAllByUserId(@NotNull final String userId, @NotNull final List<Project> projects) {
        for (@NotNull final Project project : projects)
            if (!userId.equals(project.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        projectRepository.saveAll(projects);
    }

    @NotNull
    @Override
    @Transactional
    public Project saveByUserId(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    public Project findByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return projectRepository.findFirstByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return projectRepository.findFirstByUserIdAndId(userId, id) != null;
    }

    @Override
    public long countByUserId(@NotNull final String userId) {
        return projectRepository.countByUserId(userId);
    }

    @Transactional
    public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void clear(@NotNull final List<Project> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    @Transactional
    public void clearByUserId(@NotNull final String userId) {
        projectRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void clearAllByUserId(@NotNull final String userId, @NotNull final List<Project> projects) {
        for (@NotNull final Project project : projects)
            if (!userId.equals(project.getUserId())) throw new AccessDeniedException("Error! Access denied...");
        projectRepository.deleteAll(projects);
    }

}