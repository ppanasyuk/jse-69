package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.panasyuk.tm.api.service.IAuthService;
import ru.panasyuk.tm.api.service.IUserService;
import ru.panasyuk.tm.model.Result;
import ru.panasyuk.tm.model.User;

import javax.annotation.Resource;

@Service
public final class AuthService implements IAuthService {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    public Result login(@NotNull final String username, @NotNull final String password) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @Nullable
    @Override
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userService.findByLogin(username);
    }

    @NotNull
    @Override
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}