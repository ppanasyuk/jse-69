package ru.t1.panasyuk.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getActiveMqHost();

    @NotNull
    String getActiveMqPort();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getMongoClientHost();

    @NotNull
    String getMongoClientPort();

}