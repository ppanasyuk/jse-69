package ru.t1.panasyuk.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskChangeStatusByIndexResponse;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.event.ConsoleEvent;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskChangeStatusByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    private static final String NAME = "task-change-status-by-index";

    @Override
    @EventListener(condition = "@taskChangeStatusByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS:");
        System.out.println(Status.getDisplayNames());
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken(), index, status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}