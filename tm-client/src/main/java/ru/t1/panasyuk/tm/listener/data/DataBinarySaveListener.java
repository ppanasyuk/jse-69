package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataBinarySaveRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @NotNull
    private static final String NAME = "data-save-bin";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getToken());
        domainEndpoint.saveDataBinary(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
