package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ClientConfiguration;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.marker.UnitCategory;

import javax.persistence.EntityManagerFactory;

@Category(SoapCategory.class)
@DisplayName("Тестирование сервиса PropertyService")
public class PropertyServiceTest {

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private static ApplicationContext context;

    @BeforeClass
    public static void initContext() {
        context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
    }

    @Before
    public void initService() {
        propertyService = context.getBean(IPropertyService.class);
    }

    @Test
    @DisplayName("Получение наименования файла с настройками")
    public void getApplicationConfigTest() {
        @Nullable final String value = propertyService.getApplicationConfig();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение директории для логирования")
    public void getApplicationLog() {
        @Nullable final String value = propertyService.getApplicationLog();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение порта сервера")
    public void getServerPort() {
        @Nullable final String value = propertyService.getServerPort();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение хоста сервера")
    public void getServerHost() {
        @Nullable final String value = propertyService.getServerHost();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

}