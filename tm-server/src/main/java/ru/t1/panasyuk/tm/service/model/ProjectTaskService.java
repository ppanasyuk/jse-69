package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.model.IProjectTaskService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.repository.model.ProjectRepository;
import ru.t1.panasyuk.tm.repository.model.TaskRepository;

import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @Getter
    @NotNull
    @Autowired
    protected ProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    protected TaskRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @Nullable final Project project;
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        @NotNull final TaskRepository taskRepository = getTaskRepository();
        project = projectRepository.findById(userId, projectId);
        boolean isExist = project != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable Project project;
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        project = projectRepository.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.delete(project);
        return project;
    }

    @Override
    @Transactional
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Project project;
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        if (index > projectRepository.countByUserId(userId)) throw new IndexIncorrectException();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        project = projectRepository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
        removeProjectById(userId, project.getId());
        return project;
    }

    @Override
    @Transactional
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<Project> projects;
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        projects = projectRepository.findAllOrderByCreated(userId);
        if (projects == null) return;
        for (@NotNull final Project project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        @NotNull final TaskRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findById(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.save(task);
        return task;
    }

}