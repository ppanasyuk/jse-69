package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.model.ITaskService;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.model.TaskRepository;
import ru.t1.panasyuk.tm.repository.model.UserRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class TaskService extends AbstractUserOwnedService<Task, TaskRepository>
        implements ITaskService {

    @Getter
    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final UserRepository userRepository = getUserRepository();
        @NotNull final Task task = new Task();
        @Nullable final User user = userRepository.findById(userId).get();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final UserRepository userRepository = getUserRepository();
        @NotNull final Task task = new Task();
        @Nullable final User user = userRepository.findById(userId).get();
        task.setName(name);
        task.setUser(user);
        add(task);
        return task;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final TaskRepository repository = getRepository();
        result = repository.findById(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @Nullable final List<Task> models;
        @NotNull final TaskRepository repository = getRepository();
        models = repository.findAllOrderByCreated(userId);
        return models;
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<Task> models;
        @NotNull final TaskRepository repository = getRepository();
        @NotNull String field = FieldConst.FIELD_CREATED;
        if (comparator == NameComparator.INSTANCE) field = FieldConst.FIELD_NAME;
        else if (comparator == StatusComparator.INSTANCE) field = FieldConst.FIELD_STATUS;
        @NotNull org.springframework.data.domain.Sort sort = org.springframework.data.domain.Sort.by(
                org.springframework.data.domain.Sort.Direction.DESC,
                field);
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<Task> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<Task> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Task model;
        @NotNull final TaskRepository repository = getRepository();
        model = repository.findById(userId, id);
        return model;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task model;
        @NotNull final TaskRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<Task> tasks;
        @NotNull final TaskRepository repository = getRepository();
        tasks = repository.findByProjectId(userId, projectId);
        return tasks;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final TaskRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    @Transactional
    public Task removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task removedModel;
        @NotNull final TaskRepository repository = getRepository();
        removedModel = repository.findById(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public Task removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task removedModel;
        @NotNull final TaskRepository repository = getRepository();
        @NotNull Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        ;
        repository.delete(removedModel);
        return removedModel;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}