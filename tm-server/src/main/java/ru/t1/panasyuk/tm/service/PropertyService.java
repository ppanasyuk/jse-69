package ru.t1.panasyuk.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.endpoint.IConnectionProvider;
import ru.t1.panasyuk.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, IConnectionProvider {

    @Value("#{environment['activeMQ.host']}")
    private String activeMqHost;

    @Value("#{propertyService.read('buildNumber')}")
    private String applicationVersion;

    @Value("#{environment['application.config']}")
    private String applicationConfig;

    @Value("#{environment['application.log']}")
    private String applicationLog;

    @Value("#{environment['application.name']}")
    private String applicationName;

    @Value("#{propertyService.read('email')}")
    private String authorEmail;

    @Value("#{propertyService.read('developer')}")
    private String authorName;

    @Value("#{environment['database.dialect']}")
    private String dBDialect;

    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @Value("#{environment['database.hbm2ddlauto']}")
    private String dBHbm2DdlAuto;

    @Value("#{environment['database.secondLevelCacheEnabled']}")
    private String dBSecondLevelCacheEnabled;

    @Value("#{environment['database.lazyLoadNoTrans']}")
    private String dBLazyLoadNoTransEnabled;

    @Value("#{environment['database.formatSql']}")
    private String dBFormatSql;

    @Value("#{environment['database.loggingEnabled']}")
    private Boolean dBLoggingEnabled;

    @Value("#{environment['database.cacheRegionFactory']}")
    private String dBCacheRegionFactory;

    @Value("#{environment['database.regionPrefix']}")
    private String dBCacheRegionPrefix;

    @Value("#{environment['database.configFilePath']}")
    private String dBConfigFilePath;

    @Value("#{environment['database.useQueryCache']}")
    private String dBUseQueryCache;

    @Value("#{environment['database.useMinPuts']}")
    private String dBUseMinimalPuts;

    @Value("#{environment['database.username']}")
    private String dBUser;

    @Value("#{environment['database.password']}")
    private String dBPassword;

    @Value("#{environment['database.url']}")
    private String dBUrl;

    @Value("#{propertyService.read('gitBranch')}")
    private String gitBranch;

    @Value("#{propertyService.read('gitCommitId')}")
    private String gitCommitId;

    @Value("#{propertyService.read('gitCommitterName')}")
    private String gitCommitterName;

    @Value("#{propertyService.read('gitCommitterEmail')}")
    private String gitCommitterEmail;

    @Value("#{propertyService.read('gitCommitMessage')}")
    private String gitCommitMessage;

    @Value("#{propertyService.read('gitCommitTime')}")
    private String gitCommitTime;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private int sessionTimeout;

    @NotNull
    private static final String EMPTY_VALUE = "--//--";

    @NotNull
    public String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}